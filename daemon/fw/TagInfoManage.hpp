


#ifndef  __TAGINFOMANAGE__HPP
#define  __TAGINFOMANAGE__HPP


#include <iostream>
#include <vector>


class TagInfo{
public:
    int64_t m_retranTimes;
    int64_t m_fullRtt;
    int64_t m_lastRtt;
    TagInfo(int retranTimes, int fullRtt, int lastRtt)
    : m_retranTimes(retranTimes)
    , m_fullRtt(fullRtt)
    , m_lastRtt(lastRtt)
    {

    }

};

class TagInfoManage{
public:
    static std::vector<TagInfo> m_tagInfoQue;

};





#endif
